﻿

  
#include <iostream>
class Example
{
private :
    int x;
    int length;
    int y;
    int z;
public:
    int GetX()
    {
        return x;
    }
    void SetX(int newX)
    {
        x = newX;
    }
    int GetY()
    {
        return y;
    }
    void SetY(int newY)
    {
        y = newY;
    }
    int GetZ()
    {
        return z;
    }
    void SetZ(int newZ)
    {
        z = newZ;
    }
    int Length()
    {
        length = sqrt(x * x + y * y + z * z) ;
        return length;
    }
};



using namespace std;
int main()
{
    setlocale(LC_ALL, "RUS");
    Example temp;
    temp.SetX(3);
    temp.SetY(4);
    temp.SetZ(10);
    cout <<"x=" << temp.GetX()<<endl;
    cout <<"y=" << temp.GetY()<<endl;
    cout <<"z=" << temp.GetZ()<<endl;
    cout << "vector length=" <<  temp.Length()<<endl;
}

